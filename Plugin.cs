﻿using System.Runtime.InteropServices;
using Genius.DotNet.PDK;

namespace TestPlugin;

public static class Plugin
{
    [UnmanagedCallersOnly(EntryPoint = "on_load")]
    public static void OnLoad()
    {
        //UI.ShowMessageBox("", "Hello from dotnet plugin");
        ResourceLoader.RegisterResource("plugin_s", 42);
        Logger.Debug("Yay " + ResourceLoader.GetResource<int>("plugin_s"));
        // UI.CreatePage("test", "/test", "md.icon.account_tree", "");
        //EventSysten.Invoke("plugin event", 42);
        Logger.Info("Plugin geladen");

        Events.CoolEvent += _ => { Logger.Info(_?.ToString() ?? "problem with arg"); };


        var template = @"<Grid xmlns=""https://github.com/avaloniaui""
            xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
            <TextBlock Text=""hello"" />
        </Grid>";

        Widgets.Register("Plugin", "Minus", template, template);

        Functions.SetOutput(new PluginMetadata("Test", "0.0.1"));
    }

    public static void Main()
    {
        Functions.Init();
    }
}
